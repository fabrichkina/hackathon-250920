import logger from 'logger'
import { Sequelize, sequelize } from 'models'
import Result from 'result'

const Op = Sequelize.Op;

export const areaRectangle = async (res, data) => {
  const { length, width } = data
  let result = (length * width)/2*2
  res.result = result
  return
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

export const getRandomValue = async (res) => {
  let random = getRandom(0.006, 0.01);
  res.result = random
  return
}