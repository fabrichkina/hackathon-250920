import Result from 'result'
import { areaRectangle, getRandomValue } from 'actions/public'
import express from "express"
import { LANGUAGES } from 'utils/lists'
import validator from "validators/public"

const router = express.Router();

router.get("/area-rectangle", async (req, res, next) => {
  try {
    let result = new Result();
    await areaRectangle(result, req.query)
    res.status(result.status).send(result);

  } catch (error) {
    next(error);
  }
});

router.get("/languages", validator.get, async (req, res, next) => {
  try {
    let result = new Result()
    result.result = LANGUAGES[req.query.locale]
    res.status(result.status).send(result)
  } catch (error) {
    next(error)
  }
})

router.get("/random", async (req, res, next) => {
  try {
    let result = new Result();
    await getRandomValue(result)
    res.status(result.status).send(result);

  } catch (error) {
    next(error);
  }
});

module.exports = router