import publicHandlers from "./public"

function api(server, passport) {

  server.use("/api/v1/public", publicHandlers)

}

module.exports = api