import fs from 'fs'
import logger from 'logger'
import { sequelize, Sequelize} from 'models'
import { CustomError } from 'handle-error'

const Op = Sequelize.Op;