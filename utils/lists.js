export const LANGUAGES = {
  'hy': [
    {code: "hy", title: "հայերեն"},
    {code: "en", title: "Անգլերեն"},
    {code: "ru", title: "Ռուս"},
    {code: "fa", title: "Պարսկերեն"},
    {code: "zh", title: "Չինական"},
  ],
  'en': [
    {code: "hy", title: "Armenian"},
    {code: "en", title: "English"},
    {code: "ru", title: "Russian"},
    {code: "fa", title: "Farsi"},
    {code: "zh", title: "Chinese"},
  ],
  'ru': [
    {code: "hy", title: "Армянский"},
    {code: "en", title: "Английский"},
    {code: "ru", title: "Русский"},
    {code: "fa", title: "Фарси"},
    {code: "zh", title: "Китайский"},
  ],
  'fa': [
    {code: "hy", title: "ارمنی"},
    {code: "en", title: "انگلیسی"},
    {code: "ru", title: "روسی"},
    {code: "fa", title: "فارسی"},
    {code: "zh", title: "چینی ها"},
  ],
  'zh': [
    {code: "hy", title: "亞美尼亞人"},
    {code: "en", title: "英語"},
    {code: "ru", title: "俄語"},
    {code: "fa", title: "波斯語"},
    {code: "zh", title: "中文"},
  ]
}
