import { body, param, query } from 'express-validator'
import validate from './index'

exports.get = [[
  query('locale').not().isEmpty().trim().withMessage('Locale cannot be empty.')
], validate]